# Generating raw datasets

These can be inserted into MongoDB directly or inserted using the API explorer, available at port 3000. If using minikube, be sure to portforward that on your machine.

## Requirements

This only relies on `requests` as an external dependency.

## NOTES

* Note that the sample dataset here contains sensitive data and CAN NOT be shared outside of PSI.
* The id of the dataset contains no prefix.
* These datasets are no substitution for the real thing and are not production ready.


## Running 

1. virtualenv -p `which python3` dsc
2. source dsc/bin/activate
3. pip install requests
4. python dataset_creator.py -h


