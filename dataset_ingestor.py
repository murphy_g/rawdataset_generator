import json
import pprint
import random
import string
import argparse
import requests
import bson

ENDPOINT = "https://dacat-qa.psi.ch/api/v2/"
DS_ROUTE = "RawDatasets"
DB_ROUTE = "OrigDatablocks"

p = argparse.ArgumentParser()
p.add_argument('--token')
p.add_argument('--source', default='./output/')
args = p.parse_args()

token = args.token

if not token:
    token = input('Paste token here')
    print(token)

def ingest(path, route):
    with open(args.source + path) as f:
        data_sets = json.load(f)
        for d in data_sets:
            r = requests.post("{}/{}".format(ENDPOINT, route + '?access_token={}'.format(token)), json=d)
            print(r.text)
            break

ingest('raw_datasets.bson', DS_ROUTE)
ingest('datablocks.bson', DB_ROUTE)

